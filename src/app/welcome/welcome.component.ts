import { Component } from '@angular/core';
import {ServiceTruckService} from '../service-truck.service'

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent {
  public storageData: any;
  public appData: Array<any> = [];
  constructor(private userTruck: ServiceTruckService) {
    this.fetchData();
  }

  fetchData(): any {
    this.userTruck.getData().subscribe((resp) => {
      if (resp) {
        this.storageData = resp;
        localStorage.setItem('FAQ_List', JSON.stringify(resp));
        this.appData = Object.entries(resp);
        console.log('App Data',this.appData);
      } else {
        window.alert('The data is no longer available.!');
      }
    });
  }

}
