import { Component } from '@angular/core';
import { ServiceTruckService } from '../service-truck.service';
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FAQComponent {
  public termData :any;
  constructor(private userTruck: ServiceTruckService) {
    this.faqData();
  }
  faqData():any{
    this.userTruck.getData().subscribe((resp : any)=>{
      if(resp){
        this.termData = resp;
      } else{
        window.alert('The data is no longer available.!');
      }
    })
  }
}
