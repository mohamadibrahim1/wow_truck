import { TestBed } from '@angular/core/testing';

import { ServiceTruckService } from './service-truck.service';

describe('ServiceTruckService', () => {
  let service: ServiceTruckService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceTruckService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
