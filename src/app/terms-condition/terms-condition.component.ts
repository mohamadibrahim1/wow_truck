import { Component } from '@angular/core';
import { ServiceTruckService } from '../service-truck.service';

@Component({
  selector: 'app-terms-condition',
  templateUrl: './terms-condition.component.html',
  styleUrls: ['./terms-condition.component.scss']
})
export class TermsConditionComponent {
  public termData :any;
  constructor(private userTruck: ServiceTruckService) {
    this.termsData();
  }
  termsData():any{
    this.userTruck.getData().subscribe((resp : any)=>{
      if(resp){
        this.termData = resp;
      } else{
        window.alert('The data is no longer available.!');
      }
    })
  }
}
