import { Component } from '@angular/core';
import { ServiceTruckService } from '../service-truck.service';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss']
})
export class PrivacyPolicyComponent {
  public policyData :any;
  constructor(private userTruck: ServiceTruckService) {
    this.termsData();
  }
  termsData():any{
    this.userTruck.getData().subscribe((resp : any)=>{
      if(resp){
        this.policyData = resp;
      } else{
        window.alert('The data is no longer available.!');
      }
    })
  }
}
