import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceTruckService {
  constructor(private http: HttpClient) {}
  
  getData() {
    let url = 'https://admin-staging.wowtruck.in/customerapp/splashscreen_v2';
    return this.http.get(url);
  }

  postData(post: any) {
    let url = 'https://admin-staging.wowtruck.in/customerapp/splashscreen_v2';
    return this.http.post(url, JSON.stringify(post));
  }
}
